package com.gitlab.gerhard_piette.text_6;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.letter_1.LetterEnd;

public interface Text {

	int getLength();

	int getLetterLength(int letter) throws DefectLetter;

	LetterEnd read(int offset) throws DefectLetter, DefectOffset;

	/**
	 * @param begin
	 * @param end
	 * @return
	 */
	Text getFromTo(int begin, int end) throws DefectOffset;

	default Text getFrom(int begin) throws DefectOffset {
		return getFromTo(begin, this.getLength());
	}

	default Text getTo(int end) throws DefectOffset {
		return getFromTo(0, end);
	}

	/**
	 * @return The offset before the difference in text1. Or -off - 1.
	 */
	default int beforeDifference(int offset, Text text, int begin, int end) throws DefectLetter {
		return Offset.beforeDifference(this, offset, text, begin, end);
	}

	/**
	 * @return The offset before the difference in text1. Or -off - 1.
	 */
	default int beforeDifference(int offset, Text text) throws DefectLetter {
		return Offset.beforeDifference(this, offset, text);
	}

	/**
	 * @return The offset after the match in text1. Or -off - 1.
	 */
	default int afterMatch(int offset, Text text, int begin, int end) throws DefectLetter {
		return Offset.afterMatch(this, offset, text, begin, end);
	}

	/**
	 * @return The offset after the match in text1. Or -off - 1.
	 */
	default int afterMatch(int offset, Text text) throws DefectLetter {
		return Offset.afterMatch(this, offset, text);
	}

}
