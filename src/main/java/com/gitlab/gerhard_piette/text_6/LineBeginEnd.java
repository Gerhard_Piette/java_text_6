package com.gitlab.gerhard_piette.text_6;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class LineBeginEnd {

	/**
	 * Offset before the first letter of the line.
	 */
	public int begin;

	/**
	 * Offset after the last letter of the line including the lineEnd letter.
	 */
	public int end;

	@Override
	public String toString() {
		return begin + ":" + end;
	}

	/**
	 * @param text
	 * @param lineOffset
	 * @param lineEndLetter
	 * @return
	 * @throws DefectLetter
	 */
	public static LineBeginEnd findInText(Text text, int lineOffset, int lineEndLetter) throws DefectLetter, DefectOffset {
		var ret = new LineBeginEnd();
		var lineOff = 0;
		var off = 0;
		// Find the begin of the line.
		while (lineOff < lineOffset) {
			ret.begin = off;
			var le = text.read(off);
			off = le.end;
			if (le.letter == lineEndLetter) {
				lineOff++;
			}
		}
		// Find the end of the line.
		while (off < text.getLength()) {
			var le = text.read(off);
			off = le.end;
			if (le.letter == lineEndLetter) {
				break;
			}
		}
		ret.end = off;
		return ret;
	}

	public static LineBeginEnd findInString(String str, int lineOffset, int lineEndLetter) throws DefectLetter, DefectOffset {
		var text = new StringText(str);
		return LineBeginEnd.findInText(text, lineOffset, lineEndLetter);
	}

}
