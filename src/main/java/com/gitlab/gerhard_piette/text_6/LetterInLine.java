package com.gitlab.gerhard_piette.text_6;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class LetterInLine {

	public int letterOffset = -1;

	/**
	 * Line 1 has offset 0.
	 */
	public int lineOffset = -1;

	public int letterOffsetInLine = -1;

	public String toString() {
		return lineOffset + ":" + letterOffsetInLine;
	}

	/**
	 * @param text
	 * @param letterOffset
	 * @param lineEndLetter
	 * @return
	 */
	public static LetterInLine findInText(Text text, int letterOffset, int lineEndLetter) throws DefectLetter, DefectOffset {
		var off = 0;
		int line = 0;
		var offsetInLine = 0;
		while (off < letterOffset) {
			var le = text.read(off);
			if (le.letter == lineEndLetter) {
				line++;
				offsetInLine = 0;
			} else {
				offsetInLine++;
			}
			off = le.end;
		}
		var ret = new LetterInLine();
		ret.letterOffset = letterOffset;
		ret.lineOffset = line;
		ret.letterOffsetInLine = offsetInLine;
		return ret;
	}

	/**
	 * @param str
	 * @param letterOffset
	 * @param lineEndLetter
	 * @return
	 */
	public static LetterInLine findInString(String str, int letterOffset, int lineEndLetter) throws DefectLetter, DefectOffset {
		var stringText = new StringText(str);
		return LetterInLine.findInText(stringText, letterOffset, lineEndLetter);
	}
}
