package com.gitlab.gerhard_piette.text_6;

import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;

public class Offset {

	/**
	 * @return The offset before the difference in text1. Or -off - 1.
	 */
	public static int beforeDifference(Text text1, int offset, Text text2, int begin, int end) throws DefectLetter {
		var off1 = offset;
		var off2 = begin;
		try {
			while (off2 < end) {
				var le1 = text1.read(off1);
				var le2 = text2.read(off2);
				if (le1.letter != le2.letter) {
					return off1;
				}
				off1 = le1.end;
				off2 = le2.end;
			}
		} catch (DefectOffset ld) {
			return off1;
		}
		return -off1 - 1;
	}

	/**
	 * @return The offset before the difference in text1. Or -off - 1.
	 */
	public static int beforeDifference(Text text1, int offset, Text text2) throws DefectLetter {
		return beforeDifference(text1, offset, text2, 0, text2.getLength());
	}


	/**
	 * @return The offset after the match in text1. Or -off - 1.
	 */
	public static int afterMatch(Text text1, int offset, Text text2, int begin, int end) throws DefectLetter {
		var off1 = offset;
		var off2 = begin;
		try {
			while (off2 < end) {
				var le1 = text1.read(off1);
				var le2 = text2.read(off2);
				if (le1.letter != le2.letter) {
					return -off1 - 1;
				}
				off1 = le1.end;
				off2 = le2.end;
			}
		} catch (DefectOffset ld) {
			return -off1 - 1;
		}
		return off1;
	}

	public static int afterMatch(Text text1, int offset, Text text2) throws DefectLetter {
		return afterMatch(text1, offset, text2, 0, text2.getLength());
	}

	/**
	 * @return The offset before the difference in text1. Or -off - 1.
	 */
	public static int beforeDifference(String str1, int offset, String str2, int begin, int end) {
		var off1 = offset;
		var off2 = begin;
		while (off2 < end) {
			if (off1 >= str1.length()) {
				return off1;
			}
			var le1 = str1.charAt(off1);
			var le2 = str2.charAt(off2);
			if (le1 != le2) {
				return off1;
			}
			off1++;
			off2++;
		}
		return -off1 - 1;
	}

	/**
	 * @return The offset before the difference in text1. Or -off - 1.
	 */
	public static int beforeDifference(String str1, int offset, String str2) throws DefectLetter {
		return beforeDifference(str1, offset, str2, 0, str2.length());
	}

	/**
	 * @return The offset after the match in text1. Or -off - 1.
	 */
	public static int afterMatch(String str1, int offset, String str2, int begin, int end) {
		var off1 = offset;
		var off2 = begin;
		while (off2 < end) {
			if (off1 >= str1.length()) {
				return -off1 - 1;
			}
			var le1 = str1.charAt(off1);
			var le2 = str2.charAt(off2);
			if (le1 != le2) {
				return -off1 - 1;
			}
			off1++;
			off2++;
		}
		return off1;
	}

	public static int afterMatch(String str1, int offset, String str2) {
		return afterMatch(str1, offset, str2, 0, str2.length());
	}
	
}

