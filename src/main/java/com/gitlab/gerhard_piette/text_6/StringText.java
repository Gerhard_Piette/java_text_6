package com.gitlab.gerhard_piette.text_6;


import com.gitlab.gerhard_piette.defect_2.DefectOffset;
import com.gitlab.gerhard_piette.letter_1.DefectLetter;
import com.gitlab.gerhard_piette.letter_1.LetterEnd;

public class StringText implements Text {

	public String str = null;

	public StringText() {

	}

	public StringText(String str) {
		this.str = str;
	}

	@Override
	public int getLength() {
		return str.length();
	}


	@Override
	public int getLetterLength(int letter) throws DefectLetter {
		return Character.charCount(letter);
	}

	@Override
	public LetterEnd read(int offset) throws DefectLetter, DefectOffset {
		if (offset >= getLength()) {
			throw new DefectOffset(getLength(), offset);
		}
		int cp = str.codePointAt(offset);
		var ret = new LetterEnd(cp, offset + getLetterLength(cp));
		return ret;
	}


	@Override
	public Text getFromTo(int begin, int end) throws DefectOffset {
		var ret = new StringText();
		if (end > getLength()) {
			throw new DefectOffset(getLength(), end);
		}
		ret.str = str.substring(begin, end);
		return ret;
	}

	@Override
	public String toString() {
		return str;
	}

	@Override
	public int hashCode() {
		return str.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return str.equals(obj);
	}
}
